App.accessRule("blob:*");
App.setPreference('StatusBarOverlaysWebView', 'true');
App.setPreference('StatusBarBackgroundColor', '#008000');
// Set up resources such as icons and launch screens.
App.info({
    id: 'com.cordova.educore_auth',
    name: 'auth',
    description: 'Get education power in one button click',
    author: 'Cordova Cloud Solution Development Team',
    email: 'info@cordovacloud.com',
    website: 'cordovacloud.com',
    version: '0.0.0.3',
});

App.icons({
    // Android
    'android_ldpi': 'resources/icons/72x72.png',
    'android_mdpi': 'resources/icons/96x96.png',
    'android_hdpi': 'resources/icons/144x144.png',
    'android_xhdpi': 'resources/icons/192x192.png',
    'android_xxhdpi': 'resources/icons/192x192.png',
    'android_xxxdpi': 'resources/icons/192x192.png'
});
App.launchScreens({
    // Android
    'android_ldpi_portrait': 'resources/splash/spash_320x200.png',
    'android_ldpi_landscape': 'resources/splash/spash_480x320.png',
    'android_mdpi_portrait': 'resources/splash/spash_1024x576.png',
    'android_mdpi_landscape': 'resources/splash/spash_1024x576.png',
    'android_hdpi_portrait': 'resources/splash/spash_1280x720.png',
    'android_hdpi_landscape': 'resources/splash/spash_1280x720.png',
    'android_xhdpi_portrait': 'resources/splash/spash_1600x960.png',
    'android_xhdpi_landscape': 'resources/splash/spash_1600x960.png'

});
App.setPreference('Orientation', 'landscape');
App.accessRule('*', true);
App.accessRule('cdvfile://*', true);
//Fix for avoiding unnecessry popup display while login in device.Have to replace SERVER URL
//App.accessRule("*//http://192.168.0.41:3000");

App.setPreference('WebAppStartupTimeout', 60000);
App.setPreference("LoadUrlTimeoutValue", 60000);
App.setPreference("websecurity", "disable");
App.setPreference("ShowSplashScreenSpinner", true);
App.setPreference("AutoHideSplashScreen", false);
App.setPreference("SplashScreenDelay",15000);
App.setPreference("spinnerDialog", "loading.....");
App.setPreference("progressBar", "resources/icons/144x144.png");
App.setPreference("fullscreen", true);
App.setPreference("android-windowSoftInputMode", "stateVisible|adjustPan|adjustResize");
App.setPreference("AndroidPersistentFileLocation", "Internal");
App.setPreference("AndroidPersistentFileLocation", "Compatibility");
App.setPreference("AndroidExtraFilesystems", "files,files-external,documents,sdcard,cache,cache-external,assets,root");
App.setPreference('android-targetSdkVersion', '25');
App.setPreference("KeyboardDisplayRequiresUserAction", false);
App.setPreference("OverrideUserAgent","Mozilla/5.0 Google");